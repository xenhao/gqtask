package animals.gqtask.feature.activity

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import animals.gqtask.feature.R
import animals.gqtask.feature.activity.adapter.ListAdapter
import animals.gqtask.feature.model.AnimalsResponse
import animals.gqtask.feature.retrofit.APIClient
import animals.gqtask.feature.retrofit.APIInterface
import animals.gqtask.feature.utils.NetworkConnectionCheck
import animals.gqtask.feature.viewmodel.AnimalsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var animalInterface = APIClient.getData()?.create(APIInterface::class.java)
    lateinit var listAdapter: ListAdapter
    private var isNeedReset : Boolean = true
    lateinit var animalsViewModel : AnimalsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //  view model to retain data throughout orientation change
        animalsViewModel = ViewModelProviders.of(this).get(AnimalsViewModel::class.java)

        //  get data to start the app
        getData(false)
    }

    fun getData(isRefresh : Boolean) {
        //  network check
        NetworkConnectionCheck(object : NetworkConnectionCheck.Consumer {
            override fun accept(hasInternet: Boolean) {
                if (hasInternet) {
                    //  has internet
                    //  only call API if no existing data in ViewModel
                    if(isRefresh || animalsViewModel.listOfAnimals.isEmpty()) {
                        //  get data
                        //  API call
                        val call = animalInterface?.getData()
                        call?.enqueue(object : Callback<AnimalsResponse> {
                            override fun onResponse(call: Call<AnimalsResponse>, response: Response<AnimalsResponse>) {
                                //  hide refresh animation
                                srlLists.isRefreshing = false

                                //  http code check
                                if (response.code() == 200) {
                                    val listOfAnimals: List<AnimalsResponse.Data> = response.body()?.data!!

                                    //  create a list of animal types from the response
                                    val typeList: ArrayList<String> = ArrayList()
                                    for (item: AnimalsResponse.Data in listOfAnimals) {
                                        if (typeList.isNullOrEmpty()) {
                                            typeList.add(item.kind)
                                        } else {
                                            if (!typeList.contains(item.kind))
                                                typeList.add(item.kind)
                                        }
                                    }

                                    //  store data in ViewModel to retain through orientation change
                                    animalsViewModel.listOfAnimals = listOfAnimals
                                    animalsViewModel.listOfAnimalsTypes = typeList

                                    //  create lists based on how many types of animal available
                                    createLists(isRefresh, typeList, listOfAnimals)
                                } else {
                                    //  error dialog
                                    errorDialog(getString(R.string.error_title), getString(R.string.error_content))
                                }
                            }

                            override fun onFailure(call: Call<AnimalsResponse>?, t: Throwable?) {
                                //  hide refresh animation
                                srlLists.isRefreshing = false
                                //  error dialog
                                errorDialog(getString(R.string.error_title), getString(R.string.error_content))
                            }
                        })
                    } else {
                        //  create lists using existing data from ViewModel
                        createLists(isRefresh, animalsViewModel.listOfAnimalsTypes, animalsViewModel.listOfAnimals)
                    }

                    //  reset screen if needed
                    if(isNeedReset) {
                        //  reset flag
                        isNeedReset = false
                        //  hide placeholder
                        clPlaceholder.visibility = View.GONE
                        //  show lists
                        srlLists.visibility = View.VISIBLE
                        //  setup swipe down refresh
                        srlLists.setOnRefreshListener { getData(true) }
                    }
                } else {
                    //  no internet
                    //  reset flag
                    isNeedReset = true
                    //  clear view model data to get fresh data upon connection resume
                    animalsViewModel.listOfAnimals = emptyList()
                    animalsViewModel.listOfAnimalsTypes.clear()
                    //  hide lists
                    srlLists.visibility = View.GONE
                    //  show placeholder
                    clPlaceholder.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun createLists(isRefresh: Boolean, animalTypes: ArrayList<String>, animalList: List<AnimalsResponse.Data>){
        listAdapter = ListAdapter(this@MainActivity, animalTypes, animalList)
        //  reset adapter only if needed
        if (isRefresh || rvLists.adapter != null)
            listAdapter.notifyDataSetChanged()
        else
            rvLists.adapter = listAdapter
    }

    private fun errorDialog(title: String, content: String) {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setCancelable(false)
        builder.setTitle(title)
        builder.setMessage(content)
        builder.setPositiveButton(
            android.R.string.ok
        ) { dialog, which ->
            when (which) {
                Dialog.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                }
            }
        }
        builder.show()
    }

    //  no internet refresh button
    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvRetry -> getData(false)
        }
    }
}