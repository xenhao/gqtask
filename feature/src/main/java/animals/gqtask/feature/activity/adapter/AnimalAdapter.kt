package animals.gqtask.feature.activity.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import animals.gqtask.feature.R
import animals.gqtask.feature.model.AnimalsResponse
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.list_item.view.*

class AnimalAdapter
    (private val applicationContext: Context, listOfAnimals: List<AnimalsResponse.Data>, flag: String) :
    RecyclerView.Adapter<AnimalAdapter.ViewHolder>() {

    private var mContext: Context = applicationContext
    private var filteredList: ArrayList<AnimalsResponse.Data> = ArrayList()

    init {
        //  create list of animal based on animal type passed in via flag param
        for (item: AnimalsResponse.Data in listOfAnimals) {
            if (flag.equals(item.kind, true))
                filteredList.add(item)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.list_item, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        //  set Integer max as max value for infinite scroll
        return Int.MAX_VALUE
    }

    override fun getItemId(position: Int): Long {
        //  return 0 if list is empty
        if (filteredList.size == 0)
            return 0

        return super.getItemId(position % filteredList.size)
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        //  return if list supplied is empty
        if (filteredList.size == 0)
            return

        //  get accurate position of item to be displayed
        val position = p1 % filteredList.size
        p0.onBind(applicationContext, filteredList[position])
    }

    //  item for this list
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(applicationContext: Context, data: AnimalsResponse.Data) {
            //getting phone's width
            val displayMetrics = DisplayMetrics()
            val activity = applicationContext as Activity
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val width = displayMetrics.widthPixels
            //  set image view width
            itemView.ivImage.layoutParams.width = (width * .7).toInt()

            //  glide image display
            val requestOptions = RequestOptions()
            requestOptions.diskCacheStrategy

            Glide.with(itemView.context)
                .setDefaultRequestOptions(requestOptions)
                .load(data.image)
                .apply(requestOptions)
                .apply(RequestOptions().placeholder(applicationContext.getDrawable(R.drawable.image_placeholder))
                    .dontAnimate())
                .into(itemView.ivImage)
        }
    }
}