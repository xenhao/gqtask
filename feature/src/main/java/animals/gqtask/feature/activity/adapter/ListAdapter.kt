package animals.gqtask.feature.activity.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.*
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import animals.gqtask.feature.R
import animals.gqtask.feature.model.AnimalsResponse
import kotlinx.android.synthetic.main.list_layout.view.*


/**
 * ListAdapter is used to show list of items
 */
class ListAdapter(
    private val applicationContext: Context,
    private val typeList: ArrayList<String>,
    private val fullList: List<AnimalsResponse.Data>
) : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(applicationContext).inflate(R.layout.list_layout, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return typeList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.onBind(typeList[p1], applicationContext, fullList)
    }

    //  item for this list
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //binding
        fun onBind(
            data: String,
            applicationContext: Context,
            fullList: List<AnimalsResponse.Data>
        ) {
            // list title
            itemView.tvTitle.text = data.capitalize()
            //  create animal image list
            val animalAdapter =
                AnimalAdapter(applicationContext, fullList, data)
            itemView.rvList.adapter = animalAdapter
            //  get phone screen width
            val displayMetrics = DisplayMetrics()
            val activity = applicationContext as Activity
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val width = displayMetrics.widthPixels
            //  set default list position with 1/8 screen width offset
            val layoutManager = itemView.rvList.layoutManager as LinearLayoutManager
            layoutManager.scrollToPositionWithOffset(Int.MAX_VALUE / 2, width/8)
            //  helper to snap objects to center of view
            val snapHelper = LinearSnapHelper()
            snapHelper.attachToRecyclerView(itemView.rvList)
        }
    }
}