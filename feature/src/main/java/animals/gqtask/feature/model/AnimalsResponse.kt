package animals.gqtask.feature.model

import com.google.gson.annotations.SerializedName

data class AnimalsResponse(
    @SerializedName("data")
    val `data`: List<Data>
) {
    data class Data(
        @SerializedName("image")
        val image: String,
        @SerializedName("kind")
        val kind: String
    )
}