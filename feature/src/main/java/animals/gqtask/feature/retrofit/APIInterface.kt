package animals.gqtask.feature.retrofit

import animals.gqtask.feature.model.AnimalsResponse
import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {

    @GET("/animals-data")
    fun getData(): Call<AnimalsResponse>
}